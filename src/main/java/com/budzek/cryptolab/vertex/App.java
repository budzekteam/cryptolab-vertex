package com.budzek.cryptolab.vertex;

import com.budzek.cryptolab.vertex.domain.DataInfo;
import com.budzek.cryptolab.vertex.exchange.providers.BittrexExchangeProviderVertex;
import com.budzek.cryptolab.vertex.exchange.providers.ExchangeProviderVertex;
import com.budzek.cryptolab.vertex.operations.PriceAvarageCalcVertex;
import io.vertx.core.Vertx;

public class App
{
  public static void main(String[] args) {
    Vertx vertx = Vertx.vertx();
//    vertx.deployVerticle(new MainVerticle());

    ExchangeProviderVertex exchangeProvider = new BittrexExchangeProviderVertex("USD-BTC");
    vertx.deployVerticle(exchangeProvider);
    vertx.deployVerticle(new PriceAvarageCalcVertex(exchangeProvider.publishingAddress(DataInfo.Ticker)));

  }
}
