package com.budzek.cryptolab.vertex.exchange.providers;

import com.budzek.cryptolab.vertex.domain.DataInfo;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.RequestOptions;
import io.vertx.core.json.JsonObject;

public abstract class ExchangeProviderVertex extends AbstractVerticle {

  HttpClient client;

  protected final String host;
  protected final String exchange;
  protected final String market;

  private RequestOptions baseRequestOptions;
  private RequestOptions tickerRequestOptions;


  public ExchangeProviderVertex(String exchange, String host, String market) {
    this.exchange = exchange;
    this.host = host;
    this.market = market;
  }

  @Override
  public void start(Future<Void> startFuture) throws Exception {
    initClient();

    vertx.setPeriodic(1000, i -> {
      client.getNow(tickerRequestOptions, response -> {
        System.out.println("Received response with status code " + response.statusCode());
        response.bodyHandler(buffer -> {
          vertx.eventBus().publish(publishingAddress(DataInfo.Ticker), extractTicker(buffer));
        });
      });
    });
  }

  private void initClient() {
    client = vertx.createHttpClient();
    baseRequestOptions = createBaseRequestOptions();
    tickerRequestOptions = tickerRequestOptions();
  }

  protected RequestOptions createBaseRequestOptions() {
    return new RequestOptions()
      .setPort(80)
      .setHost(host);
//      .setSsl(true);
  }

  protected RequestOptions baseRequestOptions() {
    return baseRequestOptions;
  }

  public String publishingAddress(DataInfo dataInfo) {
    return String.format("%s-%s-%s", dataInfo.name(), exchange, market);
  }

  abstract protected JsonObject extractTicker(Buffer buffer);

  abstract public RequestOptions tickerRequestOptions();
}
