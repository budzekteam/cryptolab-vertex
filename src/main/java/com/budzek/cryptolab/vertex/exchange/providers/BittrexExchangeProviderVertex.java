package com.budzek.cryptolab.vertex.exchange.providers;

import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.RequestOptions;
import io.vertx.core.json.JsonObject;

public class BittrexExchangeProviderVertex extends ExchangeProviderVertex {

  public BittrexExchangeProviderVertex(String market) {
    super("Bittrex", "bittrex.com", market);
  }

  @Override
  protected JsonObject extractTicker(Buffer buffer) {
    return buffer.toJsonObject().getJsonObject("result");
  }

  @Override
  public RequestOptions tickerRequestOptions() {
    return baseRequestOptions().setURI("/api/v1.1/public/getticker?market=" + market);
  }

}
