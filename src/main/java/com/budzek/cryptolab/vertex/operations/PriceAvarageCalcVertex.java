package com.budzek.cryptolab.vertex.operations;

import com.budzek.cryptolab.vertex.exchange.providers.ExchangeProviderVertex;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.eventbus.Message;
import io.vertx.core.http.RequestOptions;

public class PriceAvarageCalcVertex extends AbstractVerticle {

  protected String address;

  public PriceAvarageCalcVertex(String address) {
    this.address = address;
  }

  @Override
  public void start(Future<Void> startFuture) throws Exception {
    vertx.eventBus().consumer(address).handler(msg -> {

      System.out.println("Received on bus : " + msg.body());
    });
  }

}
